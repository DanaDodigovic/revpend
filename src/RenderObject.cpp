#include "RenderObject.hpp"

RenderObject::RenderObject(ShaderProgram& program,
                           const glm::mat4& proj,
                           const glm::mat4& view,
                           RenderPrimitive render_primitive)
    : render_primitive(render_primitive),
      program(program),
      proj(proj),
      view(view)
{}

void RenderObject::draw(Renderer& renderer)
{
    program.bind();
    program.set_uniform_mat4f("MVP", proj * view * model);
    renderer.set_primitive(render_primitive);
    renderer.draw(va, *ib, program);
}
