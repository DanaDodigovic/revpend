#include "System.hpp"
#include "INIReader.h"

#include <iostream>

System::System()
    : cart(ds.proj, ds.view, ds.program),
      stars(ds.proj, ds.view, ds.program, ds.borders)
{
    INIReader reader("res/parameters/parameters.ini");

    if (reader.ParseError() != 0) {
        throw std::runtime_error("Can't load given file.");
    }

    s.angle = reader.GetReal("pendulum", "angle", -1);
    pendulum =
        std::make_unique<Pendulum>(ds.proj, ds.view, s.angle, ds.program);

    solver = std::make_unique<Solver>(reader.GetReal("cart", "mass", -1),
                                      reader.GetReal("pendulum", "mass", -1),
                                      reader.GetReal("pendulum", "length", -1));

    force                    = reader.GetReal("cart", "force", -1);
    std::string control_type = reader.Get("cart", "control", "UNKNOWN");

    if (control_type == "GRADIENT") {
        type = 1;
    } else {
        type = 0;
    }

    s.x_cart      = 0.0;
    s.angular_vel = 0.0;
}

void System::draw()
{
    cart.draw(renderer);
    pendulum->draw(renderer);
    stars.draw(renderer);
}

void System::clear()
{
    renderer.clear();
}

void System::calculate_new_state(float current_force)
{
    std::array<float, 4> new_state = solver->calculate_new_state(
        {s.angle, s.angular_vel, s.x_cart, s.x_cart_vel}, current_force);
    s.angle       = new_state[0];
    s.angular_vel = new_state[1];
    s.x_cart      = new_state[2];
    s.x_cart_vel  = new_state[3];

    if (std::abs(s.angle) >= 3.14 / 2) {
        throw std::runtime_error("Game over.");
    }

    cart.set_position(s.x_cart);
    pendulum->set_position_and_angle(s.x_cart, s.angle);

    // Camera movement due to cart being close to the edge of the screen.
    constexpr float padding = 0.1f;
    float cart_reach_right  = s.x_cart + 0.2f + padding;
    float cart_reach_left   = s.x_cart - 0.2f - padding;
    if (cart_reach_right > ds.borders.right) {
        ds.view = glm::translate(
            ds.view,
            glm::vec3(-(cart_reach_right - ds.borders.right), 0.0f, 0.0f));
        ds.borders.left += cart_reach_right - ds.borders.right;
        ds.borders.right = cart_reach_right;
    } else if (cart_reach_left < ds.borders.left) {
        ds.view = glm::translate(
            ds.view,
            glm::vec3(-(cart_reach_left - ds.borders.left), 0.0f, 0.0f));
        ds.borders.right += cart_reach_left - ds.borders.left;
        ds.borders.left = cart_reach_left;
    }
}
