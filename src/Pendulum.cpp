#include "Pendulum.hpp"

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

#include <array>

Pendulum::Pendulum(const glm::mat4& proj,
                   const glm::mat4& view,
                   float initial_angle,
                   ShaderProgram& program)
    : ball(glm::vec2(0.0f, 0.0f), 1.0f, program, proj, view),
      rod(program, proj, view)
{
    // Set initial pendulum position and angle.
    set_position_and_angle(0.0f, initial_angle);

    program.bind();
    program.set_uniform3f("color", glm::vec3(1.0f, 1.0f, 1.0f));
}

void Pendulum::draw(Renderer& renderer)
{
    rod.draw(renderer);
    ball.draw(renderer);
}

void Pendulum::set_position_and_angle(float x_cart, float angle)
{
    // Calculate new model matrix based on new cart position and pendulum angle.
    ball.set_position_and_angle(x_cart, angle);
    rod.set_position_and_angle(x_cart, angle);
}

Pendulum::Ball::Ball(const glm::vec2& center,
                     float radius,
                     ShaderProgram& program,
                     const glm::mat4& proj,
                     const glm::mat4& view)
    : RenderObject(program, proj, view, RenderPrimitive::TRIANGLE_FAN)
{
    constexpr int n_segments = 360;
    std::vector<glm::vec2> pos;
    pos.reserve(n_segments + 1);
    pos.push_back(center);
    for (int i = 0; i < n_segments; i++) {
        float alpha = 2.0f * 3.1415926f * static_cast<float>(i) / n_segments;
        float x     = center.x + radius * std::cos(alpha);
        float y     = center.y + radius * std::sin(alpha);
        pos.emplace_back(x, y);
    }
    vb =
        std::make_unique<VertexBuffer>(&pos[0], pos.size() * sizeof(glm::vec2));

    std::vector<unsigned> ind(n_segments + 2);
    for (size_t i = 0; i < ind.size(); i++) ind[i] = i;
    ind.back() = ind[1];

    ib = std::make_unique<IndexBuffer>(&ind[0], ind.size());

    VertexBufferLayout layout;
    layout.push<float>(2);
    va.add_buffer(*vb, layout);
}

void Pendulum::Ball::set_position_and_angle(float x_cart, float angle)
{
    // Calculate new model matrix based on new cart position and pendulum angle.
    model = glm::translate(glm::mat4(1.0f), glm::vec3(x_cart, 0.0f, 0.0f));
    model = glm::rotate(model, -angle, glm::vec3(0.0f, 0.0f, 1.0f));
    model = glm::translate(model, glm::vec3(0.0f, 0.5f, 0.0f));
    model = glm::scale(model, glm::vec3(0.05f, 0.05f, 0.0f));
}

Pendulum::Rod::Rod(ShaderProgram& program,
                   const glm::mat4& proj,
                   const glm::mat4& view)
    : RenderObject(program, proj, view, RenderPrimitive::LINE)
{
    std::array<glm::vec2, 2> pos = {glm::vec2(0.0f, 0.0f),
                                    glm::vec2(0.0f, 1.0f)};
    std::array<unsigned, 2> ind  = {0, 1};

    ib = std::make_unique<IndexBuffer>(&ind[0], ind.size());

    vb =
        std::make_unique<VertexBuffer>(&pos[0], pos.size() * sizeof(glm::vec2));

    VertexBufferLayout layout;
    layout.push<float>(2);
    va.add_buffer(*vb, layout);
}

void Pendulum::Rod::set_position_and_angle(float x_cart, float angle)
{
    // Calculate new model matrix based on new cart position and pendulum angle.
    model = glm::translate(glm::mat4(1.0f), glm::vec3(x_cart, 0.0f, 0.0f));
    model = glm::rotate(model, -angle, glm::vec3(0.0f, 0.0f, 1.0f));
    model = glm::scale(model, glm::vec3(1.0f, 0.5f, 0.0f));
}
