#include "VertexArray.hpp"

VertexArray::VertexArray()
{
    glGenVertexArrays(1, &id);
}

VertexArray::~VertexArray()
{
    glDeleteVertexArrays(1, &id);
}

void VertexArray::add_buffer(const VertexBuffer& vb,
                             const VertexBufferLayout& layout)
{
    bind(); // Bind this vertex array.
    vb.bind();
    const auto& elements = layout.get_elements();
    unsigned offset      = 0;
    for (size_t i = 0; i < elements.size(); i++) {
        const auto& element = elements[i];
        glEnableVertexAttribArray(i);
        glVertexAttribPointer(i,
                              element.count,
                              element.type,
                              element.normalized ? GL_TRUE : GL_FALSE,
                              layout.get_stride(),
                              reinterpret_cast<const void*>(offset));
        offset +=
            element.count * VertexBufferElement::get_size_of_type(element.type);
    }
}

void VertexArray::bind() const
{
    glBindVertexArray(id);
}

void VertexArray::unibind() const
{
    glBindVertexArray(0);
}
