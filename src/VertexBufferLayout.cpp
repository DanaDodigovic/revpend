#include "VertexBufferLayout.hpp"

#include <stdexcept>

unsigned int VertexBufferElement::get_size_of_type(unsigned type)
{
    switch (type) {
        case GL_FLOAT: return sizeof(float);
        case GL_UNSIGNED_INT: return sizeof(unsigned);
        case GL_INT: return sizeof(int);
        case GL_UNSIGNED_BYTE: return sizeof(unsigned char);
    }
    throw std::runtime_error("Type not supported.");
}

template<>
void VertexBufferLayout::push<float>(unsigned count)
{
    elements.push_back({GL_FLOAT, count, false});
    stride += count * VertexBufferElement::get_size_of_type(GL_FLOAT);
}

template<>
void VertexBufferLayout::push<unsigned>(unsigned count)
{
    elements.push_back({GL_UNSIGNED_INT, count, true});
    stride += count * VertexBufferElement::get_size_of_type(GL_UNSIGNED_INT);
}

template<>
void VertexBufferLayout::push<int>(unsigned count)
{
    elements.push_back({GL_INT, count, true});
    stride += count * VertexBufferElement::get_size_of_type(GL_INT);
}

template<>
void VertexBufferLayout::push<unsigned char>(unsigned count)
{
    elements.push_back({GL_UNSIGNED_BYTE, count, true});
    stride += count * VertexBufferElement::get_size_of_type(GL_UNSIGNED_BYTE);
}
