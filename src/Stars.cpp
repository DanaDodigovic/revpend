#include "Stars.hpp"

#include "System.hpp"

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

#include <vector>
#include <random>

Stars::Stars(const glm::mat4& proj,
             const glm::mat4& view,
             ShaderProgram& program,
             const ViewBorders& borders,
             unsigned n_screens,
             unsigned n_stars)
    : RenderObject(program, proj, view, RenderPrimitive::POINT),
      x(0.0f),
      borders(borders),
      start_left(-static_cast<float>(n_screens)),
      start_right(0.0f),
      n_screens(n_screens)
{
    // Create random vertex buffer - make stars random.
    std::random_device dev;
    std::mt19937 rng(dev());
    std::uniform_real_distribution<float> dist_w(0.0f, n_screens);
    std::uniform_real_distribution<float> dist_h(-0.2f, 0.8f);
    std::vector<glm::vec2> pos;
    pos.reserve(n_stars);
    for (size_t i = 0; i < n_stars; i++) {
        pos.emplace_back(dist_w(rng), dist_h(rng));
    }
    vb =
        std::make_unique<VertexBuffer>(&pos[0], pos.size() * sizeof(glm::vec2));

    // Create index buffer.
    std::vector<unsigned> ind(n_stars);
    for (size_t i = 0; i < n_stars; i++) ind[i] = i;
    ib = std::make_unique<IndexBuffer>(&ind[0], ind.size());

    VertexBufferLayout layout;
    layout.push<float>(2); // Vertex buffer has 2 integers.
    va.add_buffer(*vb, layout);

    program.bind();
    program.set_uniform3f("color", glm::vec3(1.0f, 1.0f, 1.0f));
}

void Stars::draw(Renderer& renderer)
{
    if (borders.right >= start_right + n_screens) {
        start_left += n_screens;
        start_right += n_screens;
    } else if (borders.left <= start_left) {
        start_left -= n_screens;
        start_right -= n_screens;
    }

    model = glm::translate(glm::mat4(1.0f), glm::vec3(start_left, 0.0f, 0.0f));
    RenderObject::draw(renderer);
    model = glm::translate(glm::mat4(1.0f), glm::vec3(start_right, 0.0f, 0.0f));
    RenderObject::draw(renderer);
}

