#include "Solver.hpp"

#include <boost/numeric/odeint.hpp>

Solver::Solver(float cart_mass, float ball_mass, float rod_length)
    : cart_mass(cart_mass), ball_mass(ball_mass), rod_length(rod_length)
{}

std::array<float, 4>
Solver::calculate_new_state(std::array<float, 4> current_state, float force)
{
    this->force = force; // Set force used by operator().

    // Integration changes current state object.
    boost::numeric::odeint::integrate(*this, current_state, 0.0, 0.002, 0.002);

    return current_state;
}

void Solver::operator()(const std::array<float, 4>& s,
                        std::array<float, 4>& dxdt,
                        const double /* t */)
{
    constexpr float g = 9.80665f;
    dxdt              = {
        s[1],
        static_cast<float>(
            (force * std::cos(s[0]) - (cart_mass + ball_mass) * g * sin(s[0]) +
             ball_mass * rod_length * std::sin(s[0]) * std::cos(s[0]) *
                 std::pow(s[1], 2)) /
            (ball_mass * rod_length * std::pow(std::cos(s[0]), 2) -
             (cart_mass + ball_mass) * rod_length)),
        s[3],
        static_cast<float>(
            (force +
             ball_mass * rod_length * std::sin(s[0]) * std::pow(s[1], 2) -
             ball_mass * g * std::sin(s[0]) * std::cos(s[0])) /
            (cart_mass + ball_mass - ball_mass * std::pow(std::cos(s[0]), 2)))};
}
