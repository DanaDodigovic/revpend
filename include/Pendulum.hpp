#ifndef PENDULUM_HPP
#define PENDULUM_HPP

#include "VertexBufferLayout.hpp"
#include "RenderObject.hpp"
#include "VertexArray.hpp"
#include "IndexBuffer.hpp"
#include "Renderer.hpp"
#include "Shader.hpp"

#include <memory>

class Pendulum {
  private:
    class Ball : public RenderObject {
      public:
        Ball(const glm::vec2& center,
             float radius,
             ShaderProgram& program,
             const glm::mat4& proj,
             const glm::mat4& view);
        void set_position_and_angle(float x_cart, float angle);
    };

    class Rod : public RenderObject {
      public:
        Rod(ShaderProgram& program,
            const glm::mat4& proj,
            const glm::mat4& view);
        void set_position_and_angle(float x_cart, float angle);
    };

    Ball ball;
    Rod rod;

  public:
    Pendulum(const glm::mat4& proj,
             const glm::mat4& view,
             float initial_angle,
             ShaderProgram& program);
    void draw(Renderer& renderer);
    void set_position_and_angle(float x_cart, float angle);
};

#endif
