#ifndef SYSTEM_HPP
#define SYSTEM_HPP

#include "Pendulum.hpp"
#include "Cart.hpp"
#include "Solver.hpp"
#include "Stars.hpp"

#include <glm/gtc/matrix_transform.hpp>

struct ViewBorders {
    float left  = -1;
    float right = 1;
};

/* Global draw settings for all objects that are rendered. */
struct DrawSettings {
    glm::mat4 proj        = glm::ortho(-1.0f, 1.0f, -0.2f, 0.8f, -1.0f, 1.0f);
    glm::mat4 view        = glm::mat4(1.0f);
    ShaderProgram program = ShaderProgram("res/shaders/VertexShader.glsl",
                                          "res/shaders/FragmentShader.glsl");
    ViewBorders borders;
};

class System {
  public:
    struct State {
        float x_cart;
        float x_cart_vel;
        float angle; // Angle between y-axis and rod.
        float angular_vel;
    };

  private:
    DrawSettings ds;
    Renderer renderer;
    Cart cart;
    Stars stars;
    std::unique_ptr<Pendulum> pendulum;
    std::unique_ptr<Solver> solver;
    State s;
    float force;
    bool type;

  public:
    System();
    void draw();
    void clear();
    void translate(const glm::vec3& vec);
    void rotate(float angle, const glm::vec3& vec);
    void calculate_new_state(float current_force);

    inline Solver& get_solver() { return *solver; }
    inline const State& get_state() { return s; }
    inline bool get_type() const { return type; }
    inline float get_force() const { return force; }
};

#endif
