#ifndef STARTS_HPP
#define STARTS_HPP

#include "RenderObject.hpp"

struct ViewBorders;

class Stars : public RenderObject {
  private:
    float x;
    const ViewBorders& borders;
    float start_left;
    float start_right;
    unsigned n_screens;

  public:
    Stars(const glm::mat4& proj,
          const glm::mat4& view,
          ShaderProgram& program,
          const ViewBorders& borders,
          unsigned n_screens = 4u,
          unsigned n_stars   = 150u);

    void draw(Renderer& renderer) override;
};

#endif
