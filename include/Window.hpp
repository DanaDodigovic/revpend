#ifndef WINDOW_HPP
#define WINDOW_HPP

#include <GL/glew.h> // Has to be included before GLFW.
#include <GLFW/glfw3.h>

#include <string>
#include <memory>

class Window {
  private:
    GLFWwindow* window;
    double x;
    double y;

  public:
    Window(unsigned width,
           unsigned height,
           const std::string& title,
           bool enable_vsync = false);
    ~Window();

    bool open() const;

    void swap_buffers() const;
    void poll_events() const;
    void set_mouse_button_callback(GLFWmousebuttonfun callback) const;
    void set_keyboard_callback(GLFWkeyfun callback) const;

    std::pair<unsigned, unsigned> dimensions() const;

    /* Only one window object is wanted. Prevent copies. */
    Window(const Window& other) = delete;
    void operator=(const Window& other) = delete;
};

#endif
