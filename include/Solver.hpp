#ifndef SOLVER_HPP
#define SOLVER_HPP

#include <glm/glm.hpp>

#include <functional>

class Solver {
  private:
    // Constants for differential equations.
    const float cart_mass;
    const float ball_mass;
    const float rod_length;
    float force;

  public:
    Solver(float cart_mass, float ball_mass, float rod_length);

    std::array<float, 4> calculate_new_state(std::array<float, 4> current_state,
                                             float force);

    void operator()(const std::array<float, 4>& s,
                    std::array<float, 4>& dxdt,
                    const double /* t */);
};

#endif
