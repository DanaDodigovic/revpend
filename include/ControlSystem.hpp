#ifndef CONTROL_SYSTEM_HPP
#define CONTROL_SYSTEM_HPP

#include "Window.hpp"
#include "Solver.hpp"
#include "System.hpp"

class ControlSystem {
  public:
    virtual float calculate_force() = 0;
};

class KeyboardControl : public ControlSystem {
  private:
    float force_value;

  public:
    KeyboardControl(const Window& w, float force);
    float calculate_force() override;
};

class GradientDescent : public ControlSystem {
  private:
    System& system;
    float max_force;

  public:
    GradientDescent(System& system, float max_force);
    float calculate_force() override;
};

#endif
