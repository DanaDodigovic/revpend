#ifndef CART_HPP
#define CART_HPP

#include "RenderObject.hpp"

class Cart : public RenderObject {
  public:
    Cart(const glm::mat4& proj, const glm::mat4& view, ShaderProgram& program);
    void set_position(float x_cart);
};

#endif
