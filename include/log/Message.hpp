#ifndef MESSAGE_HPP
#define MESSAGE_HPP

#include <string>
#include <ostream>

#include "Color.hpp"

namespace Log {

class Message {
private:
    std::string content; 
    std::string preamble;
    ColorModifier mod; 
public:
    Message(const std::string& content,
            const std::string& preamble,
            ColorCode color_code);

    virtual ~Message();

    friend std::ostream& operator<< (std::ostream& s, const Message& m);

private:
    static constexpr unsigned chars_per_line = 80u;
};


class Error : public Message {
public:
    Error(const std::string& content);
private:
    static constexpr char error_preamble[] = "ERROR";
};


class Warning : public Message {
public:
    Warning(const std::string& content);
private:
    static constexpr char warning_preamble[] = "WARNING";
};


class Success : public Message {
public:
    Success(const std::string& content);
private:
    static constexpr char success_preamble[] = "SUCCESS";
};

class Note : public Message {
public:
    Note(const std::string& content);
private:
    static constexpr char note_preamble[] = "NOTE";
};

}

#endif
