# PROJECT STRUCTURE:
#      ProjectRoot
#       |- src       # Source files. Can contain subdirectories.
#       |- include   # Header files. Can contain subdirectories.
#       |- obj       # Intermediate (object) files. Mirrors source tree.
#       |- res       # Project resources.
#       |- exe       # Executable. Built with 'make'.


# ------------------------- GENERAL PROJECT SETTINGS ---------------------------
EXECUTABLE = Revpend
CXX = g++

# Directory structure.
INCLUDE_DIR = include
SRC_DIR = src
OBJ_DIR = obj
# ------------------------------------------------------------------------------


# ------------------------------- COMPILER FLAGS -------------------------------
# Configuration flags.
DEBUG_FLAGS = -ggdb3 -O0
RELEASE_FLAGS = -O3 -march=native

# Preprocessor and compiler flags.
CPPFLAGS = -Wall -Wextra -pedantic-errors -MMD -MP -I$(INCLUDE_DIR)
CXXFLAGS = -std=c++17
# ------------------------------------------------------------------------------


# -------------------------------- LINKER FLAGS --------------------------------
# Linker flags and libraries to link against.
LDFLAGS =
LDLIBS = -lGL -lGLEW -lglfw
# ------------------------------------------------------------------------------


# ------------------------------ HELPER COMMANDS -------------------------------
# Directory guard. Used to create directory if a file requires it.
DIRECTORY_GUARD = @mkdir -p $(@D)
# ------------------------------------------------------------------------------


# --------------------------- SOURCE AND OBJECT FILES --------------------------
# All source files.
SRC = $(shell find $(SRC_DIR) -name '*.cpp')

# All object files.
OBJ = $(SRC:$(SRC_DIR)/%=$(OBJ_DIR)/%.o)

# Make dependency files - with paths from the project root.
DEP = $(OBJ:.o=.d)
# ------------------------------------------------------------------------------


# --------------------------------- PHONY RULES --------------------------------
# Special words. Ignore files with those names.
.PHONY: clean purge debug release
# ------------------------------------------------------------------------------


# ------------------------------ BUILDING RECIPES ------------------------------
# Debug build.
debug: CPPFLAGS += $(DEBUG_FLAGS)
debug: $(EXECUTABLE)

# Release build.
release: CPPFLAGS += $(RELEASE_FLAGS)
release: $(EXECUTABLE)

# Linking.
$(EXECUTABLE): $(OBJ)
	$(CXX) $(LDFLAGS) $^ $(LDLIBS) -o $@

# Compiling source files.
$(OBJ_DIR)/%.cpp.o: $(SRC_DIR)/%.cpp
	$(DIRECTORY_GUARD)
	$(CXX) $(CPPFLAGS) $(CXXFLAGS) -c $< -o $@
# ------------------------------------------------------------------------------


# --------------------------------- CLEANING -----------------------------------
# Remove object files.
clean:
	$(RM) $(OBJ) $(DEP)

# Remove object directories and executable files.
purge:
	$(RM) $(OBJ_DIR) -r
	$(RM) $(EXECUTABLE)
# ------------------------------------------------------------------------------


# ------------------------------ COMPILE FLAGS ---------------------------------
compile-flags:
	@printf '%s\n' $(CPPFLAGS) $(CXXFLAGS) > compile_flags.txt
# ------------------------------------------------------------------------------


# ---------------------------------- OTHER -------------------------------------
# Make source file recompile if header file that it includes has changed. This
# requires -MD flag passed to the compiler.
-include $(DEP)
# ------------------------------------------------------------------------------
